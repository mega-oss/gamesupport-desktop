package com.mega.games.support.stub

import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.mega.games.support.GameConfig
import com.mega.games.support.MegaServices

class MegaServices(private val config: GameConfig = mapOf()) : MegaServices(config) {
    private val internalResolver by lazy { InternalFileHandleResolver() }

    override fun fileResolver(): FileHandleResolver {
        return internalResolver
    }
}